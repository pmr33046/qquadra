---
title: "Sobre"
date: 2021-10-14T13:23:37-03:00
draft: false
foto1: /images/usp-logo-azul.jpg
foto2: /images/poli.jpg
foto3: /images/poli_loc.png
loc: https://www.google.com/maps/place/Escola+Polit%C3%A9cnica+da+Universidade+de+S%C3%A3o+Paulo/@-23.5577341,-46.7295292,13.7z/data=!4m5!3m4!1s0x0:0x27dcb36f1486d564!8m2!3d-23.5571644!4d-46.730234?hl=pt-BR
---

Este site é um projeto da diciplina PMR3304 - Sistemas de Informação da Escola Politecnica da Universidade de São Paulo.