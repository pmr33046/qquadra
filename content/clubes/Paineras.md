---
title: "Clube Paineras do Morumbi"
date: 2021-10-14T15:03:43-03:00
foto1: /images/paineiras1.jpg
foto2: /images/paineiras2.jfif
foto3: /images/paineiras_loc.png
loc: https://www.google.com/maps/place/Clube+Paineiras+do+Morumby/@-23.5990352,-46.705162,15z/data=!4m2!3m1!1s0x0:0xaccd7ddc70cb78ab?sa=X&ved=2ahUKEwjirfruwd7zAhX-HLkGHaZ1BBkQ_BJ6BAhJEAU
draft: false
---

### Descrição

Fundado em 1960, o Clube Paineiras Morumby está localizado em um dos bairros mais nobres da cidade de São Paulo, o Morumbi. Seus 120 mil m² abrigam uma reserva de mata nativa, um vasto complexo esportivo e diversas opções de atividades sociais. Considerado um oásis de tranquilidade e segurança em meio ao caos urbano, é o espaço ideal para programas familiares e muita diversão. Dentro de suas construções em estilo brutalista, referência em arquitetura dos anos 60, o  Clube abriga instalações e equipamentos de última geração, oferecendo aos seus associados e dependentes, muito conforto e tranquilidade para a prática de suas atividades esportivas.

O Clube Paineiras do Morumby oferece ainda inúmeras atividades socioculturais. Visando um melhor convívio social e familiar, além de tornar os dias dos associados ainda mais divertidos e culturais. Para atender ao seu quadro associativo, composto de aproximadamente 25 mil pessoas, o Clube Paineiras do Morumby conta com instalações de ponta e um moderno completo complexo esportivo, além das instalações destinadas aos eventos e atividades da área sociocultural do clube.  Seu complexo esportivo é composto por: 7 piscinas aquecidas, sendo uma olímpica, uma piscina kids e 5 piscinas sociais; 2 quadras de peteca, 4 quadras de areia, 15 quadras de tênis, 3 ginásios, pista de skate, pista de atletismo, 1 campo de futebol (medidas oficiais), 1 campo de society, fitness completo, sala de levantamento de peso olímpico, cancha de bocha, além de salas para atividades específicas como pilates, boxe, yoga, dentre outras.

### Fotos e Localização
(Obs: Clique no mapa para obter a localização)