---
title: "Esporte Clube Sírio"
date: 2021-10-14T15:17:51-03:00
foto1: /images/sirio1.jpg
foto2: /images/sirio2.jpg
foto3: /images/sirio_loc.png
loc: https://www.google.com/maps/place/Esporte+Clube+S%C3%ADrio/@-23.609846,-46.6531319,15z/data=!4m5!3m4!1s0x0:0xdb69c0c4cb523d86!8m2!3d-23.6099091!4d-46.6527609
draft: false
---

### Descrição

Se você procurar por clubes com piscina em SP, não pode faltar o Esporte Clube Sírio nesta lista. O clube foi fundado em 1917 por jovens imigrantes sírios e libaneses e teve sua primeira sede na rua do Comércio, no centro de São Paulo. O atual endereço, na Avenida Indianópolis, teve suas obras iniciadas em 1950.

Para tornar-se sócio do Sírio, é preciso preencher um formulário com informações como estado civil e número de filhos. Consulte aqui.

O Sírio atende os moradores de Moema, um dos bairros mais nobres de SP. A região tem o melhor IDH (Índice de Desenvolvimento Humano) de São Paulo e um dos maiores do Brasil. A qualidade de vida é comparável a países europeus como Dinamarca, Suécia e Noruega.

### Fotos e 
(Obs: Clique no mapa para obter a localização)