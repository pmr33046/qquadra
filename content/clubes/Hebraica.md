---
title: "Clube Hebraica"
date: 2021-10-14T15:16:37-03:00
foto1: /images/hebraica1.jpg
foto2: /images/hebraica2.jpg
foto3: /images/hebraica_loc.png
loc: https://www.google.com/maps/place/Clube+Hebraica+SP/@-23.5787999,-46.6939151,15z/data=!4m2!3m1!1s0x0:0xa0a2872eb788361f?sa=X&ved=2ahUKEwjni8jZwd7zAhXoI7kGHQxLCY8Q_BJ6BAhDEAU
draft: false
---

### Descrição

Também localizado no bairro de Pinheiros, A Hebraica é um dos clubes de SP que tem atividades culturais e sociais além das esportivas. Aos 64 anos de idade, o clube conta hoje com oito quadras de tênis, quatro piscinas e seis quadras poliesportivas.

A Hebraica tem dois teatros, um de 500 e outro de 250 lugares, e uma galeria de arte. Os sócios têm à disposição uma biblioteca e atendimento online exclusivo.

Para tornar-se sócio, é preciso retirar um termo de reserva na recepção do clube, preencher e colher as assinaturas devidas. Após devolver na secretaria-geral, é necessário aguardar ser chamado para entregar a documentação necessária e efetuar os pagamentos. Um título do clube gira em torno de R$ 70 mil, segundo o jornal Folha de S.Paulo.

### Fotos e Localização 
(Obs: Clique no mapa para obter a localização)