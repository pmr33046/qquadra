---
title: "Clube Pinheiros"
date: 2021-10-14T15:16:55-03:00
foto1: /images/pinheiros1.jpg
foto2: /images/pinheiros2.jpg
foto3: /images/pinheiros_loc.png
loc: https://www.google.com/maps/place/Esporte+Clube+Pinheiros/@-23.5779899,-46.6875309,15z/data=!4m2!3m1!1s0x0:0x5ae6b40977156774?sa=X&ved=2ahUKEwjgx9-Nwt7zAhWhEbkGHWsZDPcQ_BJ6BAhMEAU
draft: false
---

### Descrição

Um dos mais tradicionais clubes de SP é também um dos mais antigos. O Esporte Clube Pinheiros foi fundado em 1899 por um alemão com o nome de Sport Club Germania.

O clube ocupa uma área de 170.000 m² na região central da capital paulista. Dono de uma excelente infraestrutura de esporte e lazer, o Pinheiros tem 24 quadras de tênis, seis piscinas, sete ginásios, três campos de futebol, atividades sociais e cinema, por exemplo.

Há também no clube um trabalho de formação com mais de 3500 crianças de 3 a 14 anos. Não à toa, é também conhecido como o clube mais olímpico do país, pois já conquistou 12 medalhas olímpicas e 15 paralímpicas.

Para fazer parte do clube, é preciso preencher uma proposta e candidatar-se. O Pinheiros não vende títulos sociais. A venda de títulos é feita por associados que desejem vender os seus, desde que a proposta do candidato esteja aprovada.

### Fotos e Localização
(Obs: Clique no mapa para obter a localização)