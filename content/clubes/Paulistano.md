---
title: "Clube Paulistano"
date: 2021-10-14T15:17:09-03:00
foto1: /images/paulistano1.jfif
foto2: /images/paulistano2.jpeg
foto3: /images/paulistano_loc.png
loc: https://www.google.com/maps/place/Club+Athletico+Paulistano/@-23.568036,-46.668675,15z/data=!4m2!3m1!1s0x0:0xc5ad9186fb83a1eb?sa=X&ved=2ahUKEwiBnoH_wd7zAhUAHrkGHUwHAlEQ_BJ6BAhUEAU
draft: false
---

### Descrição

O Clube Atletico Paulistano também faz parte da lista de tradicionais clubes de SP. Fundado em 1900, o clube reúne atividades sociais, culturais e esportivas em seus mais de 41 mil m² no bairro do Jardim América. 

De acordo com o próprio clube, são em média 40 modalidades esportivas, cinema, teatro, shows, exposições, cursos e workshops. O ginásio foi projetado pelo arquiteto Paulo Mendes da Rocha.

Faça chuva ou faça sol, este é um clube com piscina em SP para passar o dia. O complexo aquático foi projetado na década de 50 e conta com piscinas olímpica, semiolímpica, infantil e também coberta.

### Fotos e Localização
(Obs: Clique no mapa para obter a localização)