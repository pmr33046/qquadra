---
title: "Monte Líbano"
date: 2021-10-14T15:18:13-03:00
foto1: /images/libano1.jfif
foto2: /images/libano2.jpg
foto3: /images/libano_loc.png
loc: https://www.google.com/maps/place/Clube+Atl%C3%A9tico+Monte+L%C3%ADbano/@-23.5986434,-46.6587305,15z/data=!4m2!3m1!1s0x0:0x6f82bfdf19f0a6fc?sa=X&ved=2ahUKEwj_o-Hkwd7zAhXIIbkGHVlQCl4Q_BJ6BAhKEAU
draft: false
---

### Descrição

Próximo ao Parque do Ibirapuera e ao bairro de Moema, o clube Monte Líbano foi fundado em 1934. O espaço conta com quadras, piscinas ao ar livre e cobertas, academia de ginástica e teatro.

No clube, você pode aproveitar ainda a deliciosa gastronomia libanesa. São esfihas, quibes, charutos de repolho e outros pratos tradicionais do Líbano, como arroz de carneiro com coalhada fresca.

### Fotos e Localização
(Obs: Clique no mapa para obter a localização)